package io.sodaoud.suitepadserver;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by sofiane on 2/22/17.
 */

public class MainActivity extends Activity{

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent i = new Intent(this, WebService.class);
    i.addFlags(Service.START_STICKY);
    startService(i);
    finish();
  }
}
