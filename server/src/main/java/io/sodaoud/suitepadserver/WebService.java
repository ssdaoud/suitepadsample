package io.sodaoud.suitepadserver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class WebService extends Service {

  private static final String TAG = WebService.class.getName();
  private SimpleWebServer server;

  public WebService() {
  }

  @Override public void onCreate() {
    super.onCreate();
    server = new SimpleWebServer(12345, getAssets());
    server.start();
    Log.d(TAG, "web server started");
  }

  @Override public void onDestroy() {
    super.onDestroy();
    server.stop();
    Log.d(TAG, "web server stoped");
  }

  @Nullable @Override public IBinder onBind(Intent intent) {
    return null;
  }
}
