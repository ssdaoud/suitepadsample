package io.sodaoud.suitepadserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by sofiane on 2/22/17.
 */

public class StartReciever extends BroadcastReceiver {
  @Override public void onReceive(Context context, Intent intent) {
    Intent i = new Intent(context, WebService.class);
    context.startService(i);
  }
}
