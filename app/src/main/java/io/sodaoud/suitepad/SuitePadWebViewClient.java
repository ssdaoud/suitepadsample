package io.sodaoud.suitepad;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by sofiane on 2/22/17.
 */

public class SuitePadWebViewClient extends WebViewClient {

  @TargetApi(Build.VERSION_CODES.LOLLIPOP) @Override
  public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
    if (request.getUrl().toString().equals("http://someremoteurl.com/sample.json")) {
      onClickRetrieveStudents(view);
      return null;// new WebResourceResponse()
    }
    return super.shouldInterceptRequest(view, request);
  }

  public void onClickRetrieveStudents(final View view) {
    String URL = "content://io.sodaoud.MenuProvider";

    Uri sample = Uri.parse(URL);
    final Cursor c = view.getContext().getContentResolver().query(sample, null, null, null, null);

    view.post(new Runnable() {
      @Override public void run() {
        if (c.moveToFirst()) {
          do {
            ((WebView) view).loadUrl("javascript:addItem(JSON.parse(\'{"
                + "\"name\":\""
                + c.getString(c.getColumnIndex("name"))
                + "\","
                + "\"type\":\""
                + c.getString(c.getColumnIndex("type"))
                + "\","
                + "\"price\":\""
                + c.getString(c.getColumnIndex("price"))
                + "\"}\'))");
          } while (c.moveToNext());
        }
      }
    });
  }
}
