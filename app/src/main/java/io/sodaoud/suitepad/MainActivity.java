package io.sodaoud.suitepad;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

  private WebView webView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    webView = (WebView) findViewById(R.id.webview);

    WebViewClient client = new SuitePadWebViewClient();
    webView.setWebViewClient(client);

    WebSettings webSettings = webView.getSettings();
    webSettings.setJavaScriptEnabled(true);
  }

  @Override protected void onResume() {
    super.onResume();
    webView.loadUrl("file:///android_asset/www/index.html");
  }
}
