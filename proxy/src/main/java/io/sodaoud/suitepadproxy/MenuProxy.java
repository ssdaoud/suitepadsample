package io.sodaoud.suitepadproxy;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sofiane on 2/22/17.
 */

public class MenuProxy extends ContentProvider {
  static final String URL = "content://io.sodaoud.MenuProvider/sample";

  @Override public boolean onCreate() {
    return false;
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
      String sortOrder) {
    OkHttpClient client = new OkHttpClient();

    final Call call = client.newCall(new Request.Builder().url("http://localhost:12345").build());
    Response res = null;
    try {
      res = call.execute();
    } catch (IOException e) {
      e.printStackTrace();
    }
    String[] columns = new String[] { "type", "name", "price" };

    MatrixCursor matrixCursor = new MatrixCursor(columns);

    try {
      JSONArray array = new JSONArray(res.body().string());
      for (int i = 0; i < array.length(); i++) {
        JSONObject jsonObject = array.getJSONObject(i);
        matrixCursor.addRow(new Object[] {
            jsonObject.get("name"), jsonObject.get("price"), jsonObject.get("type")
        });
      }
    } catch (JSONException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e){
      e.printStackTrace();
    }
    return matrixCursor;
  }

  @Nullable @Override public String getType(@NonNull Uri uri) {
    return null;
  }

  @Nullable @Override public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
    return null;
  }

  @Override public int delete(@NonNull Uri uri, @Nullable String selection,
      @Nullable String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
      @Nullable String[] selectionArgs) {
    return 0;
  }
}